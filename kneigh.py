from os import listdir
from PIL import Image
import PIL
import pickle

import numpy as np

folder = '/home/dayala/Work/samples/informe_001/'

classes = ['0_261691-2',
           '10_80009735-1',
           '11_80012387-5',
           '1_2227083-3',
           '12_80017437-2',
           '13_80017582-4',
           '14_80023541-0',
           '15_80024191-6',
           '16_80027621-3',
           '17_80028994-3',
           '18_80030572-8',
           '19_80080553-4',
           '2_3769899-0',
           '3_80000519-8',
           '4_80000958-4',
           '5_80001870-2',
           '6_80002201-7',
           '7_80002491-5',
           '8_80003587-9',
           '9_80004464-9'
           ]
dst_folder = '/home/dayala/Work/samples/informe_001/'  # fname='/media/sf_dev/taxyclope/imgs/80001870-2/2-1397252505699-5981.jpeg'

sizes = [
    (2500, 1750),
    (1600, 1100),
    (1300, 900),
    (1500, 1500)
]

data = {}

N = 8
SIZE = N, N

DIST_THRESHOLD = 100.0


def get_rgb(img):
    ret = []
    new_img = img.resize(SIZE, PIL.Image.BICUBIC)
    rgb_im = new_img.convert('RGB')
    for i in range(N):
        r = 0.0
        g = 0.0
        b = 0.0
        for j in range(N):
            rt, gt, bt = rgb_im.getpixel((i, j))
            r = r + rt
            g = g + gt
            b = b + bt
        ret.append(r / N)
        ret.append(g / N)
        ret.append(b / N)
    for i in range(N):
        r = 0.0
        g = 0.0
        b = 0.0
        for j in range(N):
            rt, gt, bt = rgb_im.getpixel((j, i))
            r = r + rt
            g = g + gt
            b = b + bt
        ret.append(r / N)
        ret.append(g / N)
        ret.append(b / N)
    return ret


def main():
    #    img = Image.open('/media/sf_dev/taxyclope/clase3_bk/16-1419601293525-246550.jpeg')
    #    a = get_rgb(img)

    all_samples = []
    all_labels = []

    label = 0
    first = True
    for c in classes:
        dir_tmp = "{0}/{1}".format(folder, c)
        for f in listdir(dir_tmp):
            fname = "{0}/{1}".format(dir_tmp, f)
            # print("Reading file {}".format(fname))
            img = Image.open(fname)
            a = get_rgb(img)
            if first:
                print("Longitud de a es {}".format(len(a)))
                print(a)
                first = False
            all_samples.append(a)
            all_labels.append(label)
        label += 1

    np_all_samples = np.array(all_samples)
    np.save(open("all_samples-{0}.np".format(N), "wb"), np_all_samples)

    np_all_labels = np.array(all_labels)
    np.save(open("all_labels-{0}.np".format(N), "wb"), np_all_labels)


if __name__ == "__main__":
    main()

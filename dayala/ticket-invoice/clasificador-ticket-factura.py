import os

import numpy as np
from PIL import Image

BASE_IMAGE_PATH = '/opt/taxyclope/invoice-ticket-classifier/'

SAMPLE_CLASSES = [{'directory': 'invoice', 'class_label': 1},
                  {'directory': 'ticket', 'class_label': 2},
                  ]

SIZE = 32


def get_image_rgb(image_file):
    image_features = []

    img = Image.open(image_file)
    img = img.convert('RGB')
    img = img.resize((SIZE, SIZE), Image.BICUBIC)

    for x in range(SIZE):
        rt_h = 0
        gt_h = 0
        bt_h = 0

        rt_v = 0
        gt_v = 0
        bt_v = 0

        for y in range(SIZE):
            r, g, b = img.getpixel((x, y))
            rt_h += r
            gt_h += g
            bt_h += b

            r, g, b = img.getpixel((y, x))
            rt_v += r
            gt_v += g
            bt_v += b

        image_features.append(rt_h)
        image_features.append(gt_h)
        image_features.append(bt_h)

        image_features.append(rt_v)
        image_features.append(gt_v)
        image_features.append(bt_v)

    # Aspect Ratio
    image_features.append(img.size[0] / img.size[1])

    return image_features


if __name__ == "__main__":
    sample_features = []
    sample_target_classes = []

    i = 1
    for current_sample_class in SAMPLE_CLASSES:
        current_dir = current_sample_class.get('directory')
        current_base_path = BASE_IMAGE_PATH + current_dir
        current_class_label = current_sample_class.get('class_label')

        print("{}) Reading {}...".format(str(i), current_base_path))

        for f in os.listdir(current_base_path):
            # RGB features
            current_features = get_image_rgb(current_base_path + '/' + f)
            sample_features.append(current_features)

            sample_target_classes.append(current_class_label)

        with open("ticket-invoice/taxit_classes_featured.txt", "a") as f2:
            f2.write("{};{}\n".format(current_dir, current_class_label))

        i += 1

    np.save('ticket-invoice/taxit_features', np.array(sample_features))
    np.save('ticket-invoice/taxit_target_classes', np.array(sample_target_classes))

    # print("Target Classes (Length {}): {}".format(len(sample_target_classes), sample_target_classes))

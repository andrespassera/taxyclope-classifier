import os

import numpy as np
from PIL import Image

BASE_IMAGE_PATH = '/home/dayala/PycharmProjects/taxyclope-classifier/samples/invoices/'

SAMPLE_CLASSES = [  {'directory': '80023782-0', 'class_label': 1},      # 1 asociacion_civil_chortitzer
                    {'directory': '80049372-9', 'class_label': 4},      # 2 corporacion_chaco
                    {'directory': '80004464-9', 'class_label': 5},      # 3 chortitzer
                    {'directory': '80004464-9_1', 'class_label': 6},    # 4 chortitzer_002
                    # {'directory': '967167-6', 'class_label': 9},      # comercial_cosmos
                    # {'directory': '80027706-6', 'class_label': 10},   # agrofar
                    # {'directory': '80061408-9', 'class_label': 11},     # 5 aconasa
                    # {'directory': '80024093-6', 'class_label': 12},   # petrobras_01
                    # {'directory': '80030229-0', 'class_label': 13},     # 6 hilagro_01
                    {'directory': '80033048-0', 'class_label': 14},     # 7 caja_mutual_automovilistas_01
                    # {'directory': '1485269-1', 'class_label': 15},    # atlantic
                    # {'directory': '1640712-1', 'class_label': 16},    # comercial_madera_chaco_01
                    # {'directory': '1951534-0', 'class_label': 17},    # ferreteria_yacare_01
                    # {'directory': '2572635-8', 'class_label': 18},    # taller_motorcito_01
                    {'directory': '80016754-6', 'class_label': 19},     # 8 fernheim_01
                    # {'directory': '80017582-4', 'class_label': 20},     # 9 cooperativa_neuland
                    # {'directory': '80020929-0', 'class_label': 21},     # 10 oleaginosa_raatz
                    # {'directory': '80024281-5', 'class_label': 22},     # 11 coop_multiactiva_loma_plata
                    # {'directory': '80028258-2', 'class_label': 23},   # muebleria_paraiso
                    # {'directory': '80047271-3', 'class_label': 24},   # taller_motormarck
                    # {'directory': '80061967-6', 'class_label': 25},   # probal
                    # {'directory': '80013146-0', 'class_label': 26},   # inter
                    {'directory': '80009735-1', 'class_label': 27},     # 12 ande
                    {'directory': '80024191-6', 'class_label': 28},     # 13 essap
                    # {'directory': '80016096-7', 'class_label': 29},   # retail_002
                    {'directory': '80023541-0', 'class_label': 33},     # 14 copaco
                    {'directory': '80000519-8', 'class_label': 34},     # 15 tigo_telefonia
                    {'directory': '80000519-8_star', 'class_label': 35},# 16 tigo_star
                    {'directory': '80001870-2', 'class_label': 36},     # 17 Vicente Scavone
                    {'directory': '80002201-7', 'class_label': 37},     # 18 Itau
                    {'directory': '80003587-9', 'class_label': 38},     # 19 SIBOL
                    {'directory': '80017437-2', 'class_label': 39},     # 20 NUCLEO
                    # {'directory': '80027621-3', 'class_label': 40},     # 21 CLUB CENTENARIO
                    # {'directory': '2227083-3', 'class_label': 41},      # 22 la fogonera
                    # {'directory': '80000958-4', 'class_label': 42},     # 23 coop_universitaria
                    {'directory': '80030572-8_1', 'class_label': 43},   # 24 claro_01
                    # {'directory': '80008811-5', 'class_label': 44},     # 25 monital
                    {'directory': '80099478-7', 'class_label': 45},     # 26 fundassa
                    # {'directory': '80028994-3', 'class_label': 45},     # cremasun
                    {'directory': '80002201-7_2', 'class_label': 47},     # 47 Itau electrónico
                    {'directory': '80004464-9_3', 'class_label': 48},     # 48 Chortitzer Loma Plata -Farmacia
                  ]

SIZE = 32


def get_image_rgb(image_file):
    image_features = []

    img = Image.open(image_file)
    img = img.convert('RGB')
    img = img.resize((SIZE, SIZE), Image.BICUBIC)

    for x in range(SIZE):
        rt_h = 0
        gt_h = 0
        bt_h = 0

        rt_v = 0
        gt_v = 0
        bt_v = 0

        for y in range(SIZE):
            r, g, b = img.getpixel((x, y))
            rt_h += r
            gt_h += g
            bt_h += b

            r, g, b = img.getpixel((y, x))
            rt_v += r
            gt_v += g
            bt_v += b

        image_features.append(rt_h)
        image_features.append(gt_h)
        image_features.append(bt_h)

        image_features.append(rt_v)
        image_features.append(gt_v)
        image_features.append(bt_v)

    return image_features


if __name__ == "__main__":
    sample_features = []
    sample_target_classes = []

    i = 1
    for current_sample_class in SAMPLE_CLASSES:
        current_dir = current_sample_class.get('directory')
        current_base_path = BASE_IMAGE_PATH + current_dir
        current_class_label = current_sample_class.get('class_label')

        print("{}) Reading {}...".format(str(i), current_base_path))

        for f in os.listdir(current_base_path):
            current_features = get_image_rgb(current_base_path + '/' + f)
            sample_features.append(current_features)

            sample_target_classes.append(current_class_label)

        with open("invoice-only/taxit_classes_featured.txt", "a") as f2:
            f2.write("{};{}\n".format(current_dir, current_class_label))

        i += 1

    np.save('invoice-only/taxit_features', np.array(sample_features))
    np.save('invoice-only/taxit_target_classes', np.array(sample_target_classes))

    # print("Target Classes (Length {}): {}".format(len(sample_target_classes), sample_target_classes))

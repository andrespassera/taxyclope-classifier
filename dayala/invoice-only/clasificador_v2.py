import glob

import cv2
import numpy as np
import os
import pandas as pd
import csv
import pickle
from sklearn.cluster import MiniBatchKMeans
from sklearn.neural_network import MLPClassifier

BASE_IMAGE_PATH = '/home/dayala/PycharmProjects/taxyclope-classifier/samples/invoices/'
SAMPLE_CLASSES = [  {'directory': '80023782-0', 'class_label': 1},      # 1 asociacion_civil_chortitzer
                    {'directory': '80049372-9', 'class_label': 4},      # 2 corporacion_chaco
                    {'directory': '80004464-9', 'class_label': 5},      # 3 chortitzer
                    {'directory': '80004464-9_1', 'class_label': 6},    # 4 chortitzer_002
                    {'directory': '80061408-9', 'class_label': 11},     # 5 aconasa
                    # {'directory': '80030229-0', 'class_label': 13},     # 6 hilagro_01
                    # {'directory': '80033048-0', 'class_label': 14},     # 7 caja_mutual_automovilistas_01
                    {'directory': '80016754-6', 'class_label': 19},     # 8 fernheim_01
                    # {'directory': '80017582-4', 'class_label': 20},     # 9 cooperativa_neuland
                    # {'directory': '80020929-0', 'class_label': 21},     # 10 oleaginosa_raatz
                    # {'directory': '80024281-5', 'class_label': 22},     # 11 coop_multiactiva_loma_plata
                    {'directory': '80009735-1', 'class_label': 27},     # 12 ande
                    {'directory': '80024191-6', 'class_label': 28},     # 13 essap
                    {'directory': '80023541-0', 'class_label': 33},     # 14 copaco
                    {'directory': '80000519-8', 'class_label': 34},     # 15 tigo_telefonia
                    {'directory': '80000519-8_star', 'class_label': 35},# 16 tigo_star
                    {'directory': '80001870-2', 'class_label': 36},     # 17 Vicente Scavone
                    {'directory': '80002201-7', 'class_label': 37},     # 18 Itau
                    {'directory': '80003587-9', 'class_label': 38},     # 19 SIBOL
                    {'directory': '80017437-2', 'class_label': 39},     # 20 NUCLEO
                    # {'directory': '80027621-3', 'class_label': 40},     # 21 CLUB CENTENARIO
                    # {'directory': '2227083-3', 'class_label': 41},      # 22 la fogonera
                    # {'directory': '80000958-4', 'class_label': 42},     # 23 coop_universitaria
                    {'directory': '80030572-8_1', 'class_label': 43},   # 24 claro_01
                    {'directory': '80008811-5', 'class_label': 44},     # 25 monital
                    {'directory': '80099478-7', 'class_label': 45},     # 26 fundassa
                  ]


def crop_height_image(img_source, height_percentage):
    img = cv2.imread(img_source)
    h, w = img.shape[:2]
    c_h = int(h * height_percentage)
    return img[0:c_h, 0:w]

if __name__ == "__main__":
    sample_features = []
    sample_target_classes = []

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SURF_create()
    dico = []

    i = 0
    j = 1
    IMAGES_LIMIT = 30
    CROPPED_PERCENTAGE = 0.4
    print("Step 1: Extract keypoints from each image...")
    for current_sample_class in SAMPLE_CLASSES:
        current_dir = current_sample_class.get('directory')
        current_base_path = BASE_IMAGE_PATH + current_dir
        current_class_label = current_sample_class.get('class_label')

        print("{}) Reading {}...".format(str(j), current_base_path))
        k = 1
        for f in os.listdir(current_base_path)[:IMAGES_LIMIT]:
            print("{}.{}-) Getting descriptors from {}...".format(j, k, f))
            img = crop_height_image(current_base_path + '/' + f, CROPPED_PERCENTAGE)
            # cv2.imshow('Template cropped', img)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()
            kp, des = sift.detectAndCompute(img, None)
            i += 1
            k += 1
            for d in des:
                dico.append(d)
        j += 1
    print("Step 2: Clustering...")
    k = np.size(len(SAMPLE_CLASSES)) * 10
    batch_size = np.size(i) * 3
    kmeans = MiniBatchKMeans(n_clusters=k, batch_size=batch_size, verbose=1).fit(dico)
    filename = 'kmeans.sav'
    pickle.dump(kmeans, open(filename, 'wb'))

    print("Step 3: Creation of the histograms...")
    kmeans.verbose = False
    histo_list = []
    i = 1
    for current_sample_class in SAMPLE_CLASSES:
        current_dir = current_sample_class.get('directory')
        current_base_path = BASE_IMAGE_PATH + current_dir
        current_class_label = current_sample_class.get('class_label')

        print("{}) Reading {}...".format(str(i), current_base_path))

        for f in os.listdir(current_base_path)[:IMAGES_LIMIT]:
            img = crop_height_image(current_base_path + '/' + f, CROPPED_PERCENTAGE)
            kp, des = sift.detectAndCompute(img, None)

            histo = np.zeros(k)
            nkp = np.size(kp)

            for d in des:
                idx = kmeans.predict([d])
                histo[idx] += 1 / nkp  # Because we need normalized histograms, I prefere to add 1/nkp directly

            histo_list.append(histo)
            sample_target_classes.append(current_class_label)
        i += 1
    np.save('taxit_features', np.array(histo_list))
    np.save('taxit_target_classes', np.array(sample_target_classes))

    print("Step 4: Training of the neural network...")
    X = np.array(histo_list)
    Y = sample_target_classes
    mlp = MLPClassifier(verbose=True, max_iter=600000)
    mlp.fit(X, Y)
    filename = 'mlp.sav'
    pickle.dump(mlp, open(filename, 'wb'))
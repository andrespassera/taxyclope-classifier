import numpy as np
from sklearn import cross_validation
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
import dayala.confusionMtrxPlot as plotter
from sklearn.metrics import classification_report


from PIL import Image

SIZE = 32

CURRENT_PATH = 'invoice-only/'

def get_image_rgb(image_file):
    image_features = []

    img = Image.open(image_file)
    img = img.convert('RGB')
    img = img.resize((SIZE, SIZE), Image.BICUBIC)

    for x in range(SIZE):
        rt_h = 0
        gt_h = 0
        bt_h = 0

        rt_v = 0
        gt_v = 0
        bt_v = 0

        for y in range(SIZE):
            r, g, b = img.getpixel((x, y))
            rt_h += r
            gt_h += g
            bt_h += b

            r, g, b = img.getpixel((y, x))
            rt_v += r
            gt_v += g
            bt_v += b

        image_features.append(rt_h)
        image_features.append(gt_h)
        image_features.append(bt_h)

        image_features.append(rt_v)
        image_features.append(gt_v)
        image_features.append(bt_v)

    return image_features

if __name__ == "__main__":
    features = np.load(CURRENT_PATH + 'taxit_features.npy')
    target_classes = np.load(CURRENT_PATH + 'taxit_target_classes.npy')

    print("Features (Length {}): {}".format(len(features), features))
    print("Target Classes (Length {}): {}".format(len(target_classes), target_classes))

    neigh = KNeighborsClassifier(n_neighbors=3)
    neigh.fit(features, target_classes)

    # test_sample = get_image_rgb('/home/dayala/Work/samples/clasificador001/clase4_an/16-1416228876554-98062.jpeg')
    # print(neigh.predict([test_sample]))
    # print(neigh.predict_proba([test_sample]))

    x_train, x_test, y_train, y_test = train_test_split(features, target_classes, test_size=0.4)

    # plot conf matrix
    y_pred = neigh.predict(x_test)
    cnf_matrix = confusion_matrix(y_true=y_test, y_pred=y_pred)
    np.set_printoptions(precision=2)
    # Plot non-normalized confusion matrix
    plt.figure()
    name = 'Some name'
    plotter.plot_confusion_matrix(cnf_matrix, classes=[], title=name)
    plt.show()

    classification_report(y_test, neigh.predict(x_test),
                          target_names=[])

    # clf = svm.SVC(kernel='linear', C=1).fit(x_train, y_train)
    # print(clf.score(x_test, y_test))

    neigh = KNeighborsClassifier(n_neighbors=1)
    neigh.fit(x_train, y_train)
    print(neigh.score(x_test, y_test))

    # test_sample = get_image_rgb('/home/dayala/miguelino.jpg')
    # print(neigh.predict([test_sample]))
    # print(neigh.predict_proba([test_sample]))

    cross_validation_scores = cross_val_score(neigh, features, target_classes, cv=3)
    print('Cross Validation Scores: {}'.format(cross_validation_scores))
    print('KNN Classifier Mean Score: {}'.format(cross_validation_scores.mean()))

    print()
    print('{} different classes computed.'.format(len(np.unique(target_classes))))
    print(confusion_matrix(target_classes, neigh.predict(features)))

    # plt.plot([1, 2, 3, 4])
    # plt.ylabel('some numbers')
    # plt.show()

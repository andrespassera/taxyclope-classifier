import numpy as np
from sklearn import cross_validation, svm
from sklearn.neighbors import KNeighborsClassifier

import matplotlib.pyplot as plt

labels = None
samples = None

Ns = [8]
Ks = [1, 2, 3]

graph_data = []
graph_data_scores = []

for N in Ns:
    labels = np.load("all_labels-{0}.np".format(N))
    samples = np.load("all_samples-{0}.np".format(N))

    X_train, X_test, y_train, y_test = cross_validation.train_test_split(samples, labels, test_size=0.2, random_state=0)

    errs = []
    scores = []
    for K in Ks:
        print("K value {}...".format(K))
        # neigh = KNeighborsClassifier(n_neighbors=K)
        neigh = svm.SVC(kernel='linear', C=K).fit(X_train, y_train)
        neigh.fit(X_train, y_train)
        xpred = neigh.predict(X_test)
        cerr = np.count_nonzero(y_test - xpred) / len(xpred)
        errs.append(cerr * 100)
        scores.append(neigh.score(X_test, y_test))
        # print("SCORE para n={} y k={} = {}".format(N, K, neigh.score(X_test, y_test)))

    graph_data.append(errs)
    graph_data_scores.append(scores)

plt.title("Error TAMAÑO/K")

plt.title("Error TAMAÑO/K")
plt.xlabel("K")
plt.ylabel("Err %")
i = 0
for gd in graph_data_scores:
    plt.plot(Ks, gd, label="Size {}".format(Ns[i]), marker="+")
    i = i + 1
# plt.axis(Ks)
plt.legend()
plt.show()

print(Ks)
print(graph_data)

import cv2
from os import listdir
from PIL import Image
import pytesseract
import math
import shutil
import re

folder='/media/sf_dev/taxyclope/imgs/80001870-2/'
dst_folder='/home/pablo/dev/80001870-2'
fname='/media/sf_dev/taxyclope/imgs/80001870-2/2-1397252505699-5981.jpeg'

sizes = [
    (2500, 1750),
    (1600, 1100),
    (1300, 900),
    (1500, 1500)
]

data = {}

DIST_THRESHOLD  = 100.0


txt="""
N“ DE TIMBRADO 11223694
vando 65569 02 d2 Sepilzmme 2M5
Véhdu hasla 30 de Sepllambm 2016

RUC: 80001870—2

FACTURA
N° 007—020—0042795"""

def get_timbrado(s):
    words = s.split(' ')
    timbrado = words[-1]
    try:
        int(timbrado)
        return timbrado
    except:
        return None

def get_ruc(s):
    m = re.search("\d+[-—]\d$", s)
    if m:
        return m.group(0)
    return None

def get_nro_factura(s):
    m = re.search("\d+[-— ]\d+[-— ]\d+", s)
    if m:
        return m.group(0)
    return None


def get_data(s):
    data = {}
    lines = s.split('\n')
    for line in lines:
        factura = get_nro_factura(line)
        if factura:
            data['nro_factura'] = factura
            continue
        ruc = get_ruc(line)
        if ruc:
            data['ruc'] = ruc
            continue
        timbrado = get_timbrado(line)
        if 'timbrado'  not in data and timbrado:
            data['timbrado'] = timbrado
            continue
    return data




count = 0
good = 0
bad = 0

outcsv = open("{0}/{1}".format(dst_folder, "out.csv"), "w")

for f in listdir('/media/sf_dev/taxyclope/imgs/80001870-2'):
    fname = "{0}/{1}".format(folder, f)
#    img = cv2.imread(fname)
#    print("Image size: {0}x{1} (colors: {2})".format(img.shape[0], img.shape[1], img.shape[2]))
#    crop_img = img[10:200, 840:1200] # Crop from x, y, w, h -> 100, 200, 300, 400
#    cv2.imshow("cropped", crop_img)
#    cv2.waitKey(0)
    img = Image.open(fname)
#    print(dir(img))
    ialt = img.crop((840, 10, 1200, 200))
#    ialt.show()
    s = pytesseract.image_to_string(ialt)
#    print(s)
    d = get_data(s)
    gb = 'g'
    if 'nro_factura' in d and 'timbrado' in d and 'ruc' in d:
#        print(d)
        good = good + 1
    else:
#        print("Esto no pudo leer en {1}:\n{0}".format(s, fname))
#        print(d)
        gb = 'b'
        bad = bad + 1

    outline = "{4},{0},{1},{2},{3},{5}\n".format(
        d['nro_factura'] if 'nro_factura' in d else '',
        d['timbrado'] if 'timbrado' in d else '',
        d['ruc'] if 'ruc' in d else '',
        gb,
        count,
        f
    )

    outcsv.write(outline)

    outtess = open("{0}/{1}.txt".format(dst_folder, f), "w")
    outtess.write(s)
    outtess.close()


#    ialt.show()

    count = count+1
#    if count%10 == 0:
#        print("Listo {0}".format(count))

print("TOTAL GOOD: {0} TOTAL BAD: {1} TOTAL: {2} %GOOD {3}".format(good, bad, good + bad, (good)/(good+bad)))

"""
    nearest = 10000000.0
    nearest_i = 10000
    for i in range(len(sizes)):
        sz = sizes[i]
        dist = math.hypot(img.shape[0] - sz[0], img.shape[1] - sz[1])
        if dist < nearest:
            nearest = dist
            nearest_i = i
    if nearest < DIST_THRESHOLD:
        dst_file = '{0}/{1}/{2}'.format(dst_folder, nearest_i, f)
    else:
        dst_file = '{0}/{1}/{2}'.format(dst_folder, 'outliers', f)

    shutil.copyfile(fname, dst_file)
"""








"""
img = cv2.imread(fname)
print("Image size: {0}x{1} (colors: {2})".format(img.shape[0], img.shape[1], img.shape[2]))
crop_img = img[200:400, 100:300] # Crop from x, y, w, h -> 100, 200, 300, 400
# NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
cv2.imshow("cropped", crop_img)

cv2.waitKey(0)
"""
